const db = require('../db/client')

class IssueService {
  constructor(container) {
    this.db = container.get('db')
  }

  getIssues(skip) {
    return this.db('issues').orderBy('created_at').offset(skip)
  }

  async createIssue(title, description) {
    let [id] = await this.db('issues').insert({ title, description })
    let [newIssue] = await this.db('issues').where({ id })
    return newIssue
  }

  changeIssueStatus(issueId, status) {
    return this.db('issues').where({ id: issueId }).update({ status })
  }

  getStatuses() {
    return this.db('statuses').orderBy('priority')
  }
}

module.exports = {
  IssueService
}