
exports.up = function (knex) {
  return knex.schema.createTable('statuses', (table) => {
    table.string('status', 50).primary();
    table.integer('priority').notNullable();
  })
    .then(() => {
      return knex.schema.createTable('issues', (table) => {
        table.increments('id').primary();
        table.string('title', 50).notNullable();
        table.string('description', 200).notNullable();
        table.string('status', 50).notNullable().defaultTo('opened');
        table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

        table.foreign('status').references('statuses.status');
      })
    })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('issues')
    .then(() => {
      return knex.schema.dropTableIfExists('statuses')
    })
};
