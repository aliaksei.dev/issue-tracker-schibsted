exports.notFound = (req, res) => {
  res.status(404).send('not found')
}

exports.unhadledErrors = (err, req, res, next) => {
  console.log(err)
  res.status(500).send('bad request')
}

exports.catchError = (fn) => {
  return function (req, res, next) {
    return fn(req, res, next).catch((err) => {
      next(err)
    })
  }
}