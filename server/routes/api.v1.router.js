const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser')
const { getIssues, initialRequest, changeIssueStatus, createIssue } = require('../controllers/api.v1.controller');
const { catchError, notFound, unhadledErrors } = require('./errorHandler');

router.get('/init', catchError(initialRequest))
router.get('/issues', catchError(getIssues))
router.put('/issues/:issueId', bodyParser.json(), catchError(changeIssueStatus))
router.post('/issues', bodyParser.json(), catchError(createIssue))

router.use(notFound)
router.use(unhadledErrors)

module.exports = router