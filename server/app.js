const express = require('express');
const path = require('path');
const apiRouter = require('./routes/api.v1.router');
const app = express();

app.use('/api/v1', apiRouter)

app.use(express.static(path.join(__dirname, '../frontend/build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../frontend/build/index.html'));
})

module.exports = app