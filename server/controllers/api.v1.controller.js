const { IssueService } = require('../services/issue.service')
const { Container } = require('typedi')

let issueService = Container.get(IssueService)

exports.initialRequest = async (req, res) => {
  let [issues, statuses] = await Promise.all([issueService.getIssues(), issueService.getStatuses()])
  res.send({ issues, statuses })
}

exports.getIssues = async (req, res) => {
  let { skip } = req.query
  let issues = await issueService.getIssues(skip)
  res.send({ issues })
}

exports.changeIssueStatus = async (req, res) => {
  let { issueId } = req.params
  let { status } = req.body
  console.log(req.body)
  await issueService.changeIssueStatus(issueId, status)
  res.sendStatus(200)
}

exports.createIssue = async (req, res) => {
  let { title, description } = req.body
  let data = await issueService.createIssue(title, description)
  res.send(data)
}