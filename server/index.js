const { isDevelopment } = require('./config/constants')

require('./services/global.services')

const app = require('./app')

if (isDevelopment) {
  app.set('port', 7777)
}

const server = app.listen(app.get('port'), () => {
  console.log(`Express running :: PORT ${server.address().port}`)
})