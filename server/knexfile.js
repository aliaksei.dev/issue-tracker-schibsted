// Update with your config settings.

const { DB_CONFIG, isDevelopment } = require('./config/constants')

module.exports = {
  client: 'mysql',
  connection: {
    host: 'localhost',
    user: DB_CONFIG.dbUser,
    password: DB_CONFIG.dbPassword,
    database: DB_CONFIG.dbName
  },
  debug: isDevelopment
};
