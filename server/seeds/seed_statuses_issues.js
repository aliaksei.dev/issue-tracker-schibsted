
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('issues').del()
    .then(() => knex('statuses').del())
    .then(function () {
      // Inserts seed entries
      return knex('statuses').insert([
        { priority: 1, status: 'opened' },
        { priority: 2, status: 'pending' },
        { priority: 3, status: 'closed' }
      ]);
    })
    .then(() => {
      return knex('issues').insert([
        { id: 1, title: 'Some issue 1', description: 'Some issue description' },
        { id: 2, title: 'Some issue 2', description: 'Some issue description' },
        { id: 3, title: 'Some issue 3', description: 'Some issue description' }
      ]);
    });
};
