const path = require('path')
const dotenv = require('dotenv')
const isDevelopment = process.env.NODE_ENV !== 'production'

if (isDevelopment) {
  dotenv.config({ path: path.join(__dirname, 'variables.dev.env') })
} else {
  dotenv.config({ path: path.join(__dirname, 'variables.env') })
}