require('./variables.env.js')

const isDevelopment = process.env.NODE_ENV !== 'production'

const DB_CONFIG = {
  host: 'localhost',
  port: process.env.DB_PORT,
  dbName: process.env.DB_NAME,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORD
}

module.exports = {
  isDevelopment,
  DB_CONFIG
}