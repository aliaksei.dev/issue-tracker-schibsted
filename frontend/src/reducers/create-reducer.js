export function createReducer(initialState, reducerCases) {
  return (state = initialState, action) => {
    const actionHandler = reducerCases[action.type]
    return actionHandler ? actionHandler(state, action.payload) : state
  }
}