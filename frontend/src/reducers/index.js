import { combineReducers } from 'redux'
import issues from './issues'

export const rootReducer = combineReducers({
  issues
})