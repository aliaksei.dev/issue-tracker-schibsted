import { createReducer } from './create-reducer';

import { RECEIVE_ISSUES, NEW_ISSUE, ISSUE_CHANGE_STATUS, ISSUES_INITIAL_DATA } from '../actions/issues'


let initialState = {
  loaded: false,
  statuses: [],
  items: [],
  maxPriority: null
}


export default createReducer(initialState, {

  [ISSUES_INITIAL_DATA]: (state, payload) => {
    let priorities = payload.statuses.map(({ priority }) => priority)
    const maxPriority = Math.max.apply(null, priorities)
    return Object.assign({}, state, {
      loaded: true,
      statuses: payload.statuses,
      items: payload.issues,
      maxPriority
    })
  },

  [RECEIVE_ISSUES]: (state, payload) => {
    return Object.assign({}, state, {
      items: state.items.concat(payload.issues),
    })
  },

  [NEW_ISSUE]: (state, payload) => {
    return Object.assign({}, state, {
      items: state.items.concat(payload)
    })
  },

  [ISSUE_CHANGE_STATUS]: (state, { issueId, status }) => {
    return Object.assign({}, state, {
      items: state.items.map((item) => {
        if (item.id === issueId) item.status = status
        return item
      })
    })
  }
})