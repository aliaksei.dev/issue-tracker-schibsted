import React from 'react';
import PropTypes from 'prop-types';

import { Select, MenuItem, Chip } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';

function IssueStatus({ handleChange, currentPriority, maxPriority, status, statuses }) {
  return (
    currentPriority === maxPriority ?
      <Chip
        icon={<DoneIcon />}
        label={status}
      /> :
      <Select
        onChange={handleChange}
        value={status}>
        {
          statuses.map(({ status, priority }) => (<MenuItem key={status} value={status} disabled={currentPriority > priority}>{status}</MenuItem>))
        }
      </Select>
  );
}

IssueStatus.propTypes = {
  handleChange: PropTypes.func.isRequired,
  currentPriority: PropTypes.number.isRequired,
  maxPriority: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  statuses: PropTypes.arrayOf(
    PropTypes.shape({
      status: PropTypes.string,
      priority: PropTypes.number
    })
  )
}

export default IssueStatus;