import React from 'react';
import { Paper, makeStyles, Typography } from '@material-ui/core';

import { useSelector, useDispatch } from 'react-redux';
import { changeIssueStatus } from '../../actions/issues';
import IssueStatus from './IssueStatus';


let useStyles = makeStyles((theme) => ({
  wrapper: {
    margin: theme.spacing(4, 2)
  },
  paper: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2)
  }
}))

function List() {
  let { items, statuses, maxPriority } = useSelector((state) => state.issues)

  let cls = useStyles()

  let dispatch = useDispatch()

  return (
    <div className={cls.wrapper}>
      {
        items.map((issue) => {

          let { priority: currentPriority } = statuses.find(({ status }) => status === issue.status)

          async function handleChange(e) {
            await dispatch(changeIssueStatus(issue.id, e.target.value))
          }

          return (
            <Paper
              key={issue.id}
              className={cls.paper}
              elevation={3}>
              <Typography variant="h6" gutterBottom>{issue.title}</Typography>
              <Typography variant="body1" gutterBottom>{issue.description}</Typography>
              <IssueStatus
                handleChange={handleChange}
                currentPriority={currentPriority}
                maxPriority={maxPriority}
                status={issue.status}
                statuses={statuses} />
            </Paper>
          )
        })
      }
    </div>
  );
}

export default List;