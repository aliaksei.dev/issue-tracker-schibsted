import React, { useEffect } from 'react';

import List from './List'
import { useSelector, useDispatch } from 'react-redux';
import { getInitialData } from '../../actions/issues';
import { CircularProgress, makeStyles } from '@material-ui/core';
import { sleep } from '../../utils/sleep';

let useStyles = makeStyles((theme) => ({
  progress: {
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(4)
  }
}))

function Home() {
  let loaded = useSelector((state) => state.issues.loaded)
  let dispatch = useDispatch()

  let cls = useStyles()

  useEffect(() => {
    (async function () {
      await sleep(1000)
      dispatch(getInitialData())
    })()
  }, [dispatch])

  return (
    loaded ?
      <List /> :
      <div className={cls.progress}>
        <CircularProgress />
      </div>
  );
}

export default Home;