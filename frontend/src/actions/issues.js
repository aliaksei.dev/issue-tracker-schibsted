export const RECEIVE_ISSUES = 'RECEIVE_ISSUES'
export const NEW_ISSUE = 'NEW_ISSUE'
export const ISSUE_CHANGE_STATUS = 'ISSUE_CHANGE_STATUS'
export const ISSUES_INITIAL_DATA = 'ISSUES_INITIAL_DATA'


function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms)
  })
}

export function newIssue(title, description) {
  return async (dispatch) => {
    let resp = await fetch('/api/v1/issues', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ title, description })
    })
    let newItem = await resp.json()
    dispatch({ type: NEW_ISSUE, payload: newItem })
  }
}


export function getIssues() {
  return async (dispatch) => {
    await sleep(1500)
    dispatch({ type: RECEIVE_ISSUES, payload: [] })
  }
}

export function getInitialData() {
  return async (dispatch) => {
    let resp = await fetch('/api/v1/init')
    let data = await resp.json()
    dispatch({ type: ISSUES_INITIAL_DATA, payload: data })
  }
}

export function changeIssueStatus(issueId, status) {
  return async (dispatch) => {

    await fetch(`/api/v1/issues/${issueId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ status })
    })

    dispatch({ type: ISSUE_CHANGE_STATUS, payload: { issueId, status } })
  }
}