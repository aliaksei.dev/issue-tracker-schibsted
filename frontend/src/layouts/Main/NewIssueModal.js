import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  CircularProgress
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { newIssue } from '../../actions/issues';

function NewIssueModal({ handleClose, isOpen }) {

  // fields
  let [title, setTitle] = useState('')
  let [description, setDescription] = useState('')

  let dispatch = useDispatch()
  let [isSaving, setIsSaving] = useState(false)

  async function onSubmit() {
    setIsSaving(true)
    await dispatch(newIssue(title, description))
    handleClose()
    setIsSaving(false)
  }

  return (
    <Dialog open={isOpen} onClose={handleClose} aria-labelledby="new-issue">
      <DialogTitle id="new-issue">New issue</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          onChange={(e) => setTitle(e.target.value)}
          margin="dense"
          id="title"
          label="Title"
          variant="outlined"
          fullWidth
        />
        <TextField
          onChange={(e) => setDescription(e.target.value)}
          margin="dense"
          id="description"
          label="Description"
          variant="outlined"
          multiline
          rows={4}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        {
          !isSaving && <Button onClick={handleClose} color="primary">Cancel</Button>
        }
        <Button onClick={onSubmit} color="primary">
          {
            isSaving ?
              <CircularProgress size="1rem" /> :
              'Submit'
          }
        </Button>
      </DialogActions>
    </Dialog>
  );
}

NewIssueModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
}

export default NewIssueModal;