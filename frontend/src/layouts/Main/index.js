import React from 'react';
import PropTypes from 'prop-types';

import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';

import NewIssueModal from './NewIssueModal'

const MainLayout = ({ children }) => {

  const [isOpen, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <AppBar position="sticky">
        <Toolbar>
          <Typography>
            Schibsted. Issue Tracker
          </Typography>
          <IconButton
            onClick={() => setOpen(true)}
            style={{ marginLeft: 'auto' }}
            color="inherit" >
            <CreateIcon />
          </IconButton>
          <NewIssueModal
            isOpen={isOpen}
            handleClose={handleClose} />
        </Toolbar>
      </AppBar>
      {children}
    </div>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node.isRequired
}

export default MainLayout;