# issue-tracker-schibsted

**How to run application?**

At first you should set up a MySQL server on your machine and create database "schibsted".

MySQL config (can be changed in config/variables.env):

user: "root",
password: "123"

Then, to run application perform next steps: 
1. Go to the frontend folder
2. npm i
3. Go to the backend folder
4. npm i
5. npm run configure-database
6. npm run seed
7. npm start

**Description:**

It is a simple issue-tracker

Frontend technologies: React (+ redux), Material UI

Backend: Express, knex, MySQL

Git: gitflow

**How can this be improved?**

It can be improved a lot, but as it is a simple test project we can:

Add more functionality (e.g. filtration, deleting, editing, load more).

Add validations on the backend and frontend.

It uses a single database (for dev and prod). The better option is to create three databases for each env (prod, dev, test).